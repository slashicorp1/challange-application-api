# Spring Boot, MySQL, JPA, Hibernate Rest API Tutorial

Build Restful CRUD API for a simple 'Setups' application using Spring Boot, Mysql, JPA and Hibernate.

## Requirements

1. Java JDK (v8+) (https://www.oracle.com/technetwork/java/javase/downloads/index.html)
2. Maven (v3+) (https://maven.apache.org/download.cgi)
3. MySQL

## Steps to Setup

**1. Clone the application**

```bash
git clone https://gitlab.com/slashicorp1/challange-application-api.git
```

**2.0 Running Docker Mysql Container**

```bash
$ docker run --name fiap-mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root -d mysql
```

**2.1 Create Mysql DATABASE**

```mysql
CREATE DATABASE register_establishment
```

**2.2 Create Mysql TABLE and set column id AUTO_INCREMENT**

```mysql
CREATE TABLE 'register_establishment'.'setup' (
                                        'id' INT NOT NULL,
                                        'name' VARCHAR(255) NULL,
                                        'types' VARCHAR(255) NULL,
                                        'email' VARCHAR(255) NULL,
                                        'username' VARCHAR(255) NULL,
                                        'passwd' VARCHAR(45) NULL,
                                        'phone' VARCHAR(45) NULL,
                                        'cnpj' VARCHAR(45) NULL,
                                        PRIMARY KEY ('id'));

ALTER TABLE 'register_establishment'.'setup'
    CHANGE COLUMN 'id' 'id' INT(11) NOT NULL AUTO_INCREMENT,
    ADD UNIQUE INDEX 'id_UNIQUE' ('id' ASC);
```

**3. Change mysql username and password as per your installation**

+ open `src/main/resources/application.properties`

+ change `spring.datasource.username` and `spring.datasource.password` as per your mysql installation

**4. Build and run the app using maven**

```bash
mvn package
java -jar target/setup-api.jar
```

Alternatively, you can run the app without packaging it using -

```bash
mvn spring-boot:run
```

The app will start running at <http://localhost:8080>.

## Explore Rest APIs

The app defines following CRUD APIs.

```
    GET /setups
    
    POST /setups
    
    GET /setups/{id}
    
    PUT /setups/{id}
    
    DELETE /setups/{id}
```

The POST structure JSON

```JSON
{
    "name": "Livraria da FIAP",
    "types": "Livraria",
    "email": "suporte@livrariafiap.com.br",
    "username": "suporte.user",
    "passwd": "F!@p2021",
    "phone": "(11) 11111-1111",
    "cnpj": "111111111/1111-11"
}
```

You can test them using postman or any other rest client.

