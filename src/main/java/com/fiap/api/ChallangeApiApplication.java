package com.fiap.dbe;

import com.fiap.api.model.Setup;
import com.fiap.api.repository.SetupRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.stream.LongStream;

@SpringBootApplication
public class ChallangeApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChallangeApiApplication.class, args);
    }
}
